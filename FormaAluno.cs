﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadastroEscolar
{
    public partial class FormaAluno : Form
    {
        List<Aluno> alunos;

        public FormaAluno(List<Aluno> alunos)
        {
            InitializeComponent();
            this.alunos = alunos;
        }

        private void txtNomeAluno_TextChanged(object sender, EventArgs e)
        {

        }

        private void btSalvarAluno_Click(object sender, EventArgs e)
        {
            Aluno a = new Aluno();
            a.setNome(txtNomeAluno.Text);
            a.setMatricula(txtMatricula.Text);
            a.setDataNascimento(txtDataNascimento.Text);
            a.setDataMatricula(txtDataMatricula.Text);
            alunos.Add(a);

            MessageBox.Show("Aluno salvo com sucesso", "Sucesso", MessageBoxButtons.OK);
            limparCampos();

        }

        public void limparCampos()
        {
            txtNomeAluno.Text = "";
            txtMatricula.Text = "";
            txtDataNascimento.Text = "";
            txtDataMatricula.Text = "";
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMatricula_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
