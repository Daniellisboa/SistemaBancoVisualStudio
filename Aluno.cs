﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadastroEscolar
{
    public class Aluno
    {
        private String nome;
        private String matricula;
        public String dataNascimento;
        public String dataMatricula;

        public String getNome()
        {
            return nome;
        }
        public void setNome(String nome)
        {
            this.nome = nome;
        }
        public String getMatricula()
        {
            return matricula;
        }
        public void setMatricula(String matricula)
        {
            this.matricula = matricula;
        }
        public String getDataNascimento()
        {
            return dataNascimento;
        }
        public void setDataNascimento(String dataNascimento)
        {
            this.dataNascimento = dataNascimento;
        }
        public String getDatamatriculao()
        {
            return dataMatricula;
        }
        public void setDataMatricula(String dataMatricula)
        {
            this.dataMatricula = dataMatricula;
        }

    }
}
